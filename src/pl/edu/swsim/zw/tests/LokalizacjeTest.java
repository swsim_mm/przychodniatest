package pl.edu.swsim.zw.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.edu.swsim.zw.HibernateFactory;
import pl.edu.swsim.zw.TextTools;
import pl.edu.swsim.zw.dao.LokalizacjeRep;
import pl.edu.swsim.zw.entities.MiastoEnt;
import pl.edu.swsim.zw.entities.UlicaEnt;
import pl.edu.swsim.zw.entities.WojewodztwoEnt;

public class LokalizacjeTest {
	public static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	public static Session session = null;
	public static Transaction transaction = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		logger.info("Test dla klasy " 
				+ new Object() { }.getClass().getEnclosingClass().getName());
		session = HibernateFactory.getSession();
		if(session == null)
			throw new Exception("Otwarcie sesji nie udało się");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		session.close();
	}

	@Before
	public void setUp() throws Exception {
		logger.trace(TextTools.padText("setUp", "=", 45));
		transaction = session.beginTransaction();
	}

	@After
	public void tearDown() throws Exception {
		logger.trace(TextTools.padText("tearDown", "-", 45));
		transaction.rollback();
	}


	/**
	 * Zwracanie poprawnie listy, inaczej nie ma sensu dalej robić testów
	 */
	@Test
	public void initialTest() {
		logger.trace(".");
		
		List<WojewodztwoEnt> wojList = new LokalizacjeRep().getWojewodztwa();
		assertNotNull(wojList);		
		
		List<MiastoEnt> miastList = new LokalizacjeRep().getMiasta();
		assertNotNull(miastList);
		
		List<UlicaEnt> uliceList = new LokalizacjeRep().getLokalizacje();
		assertNotNull(uliceList);
				
	}
	
	@Test
	public void wojewodztwoConstructorTest() {
		// Tu wygląda prosto ale dalej w klasach nie jest tak fajnie
		WojewodztwoEnt el = new WojewodztwoEnt();
		el.setNazwa("TestWojewodztwo");
		el.setId(101);
		WojewodztwoEnt el_compare = new WojewodztwoEnt(el);
		
		assertEquals(el.getNazwa(), el_compare.getNazwa());
		
		assertEquals(el.getId(), el_compare.getId());
	}
	
	@Test
	public void miastoConstructorTest() {
		
		WojewodztwoEnt el_w = new WojewodztwoEnt();
		el_w.setId(101);
		el_w.setNazwa("TestWojewodztwo");

		MiastoEnt el = new MiastoEnt();
		el.setId(1234);
		el.setNazwa("TestMiasto");
		el.setKod_pocztowy("12-543");
		el.setWojewodztwo(el_w);
		
		MiastoEnt el_compare = new MiastoEnt(el);
		
		assertEquals(el.getNazwa(), el_compare.getNazwa());
		assertEquals(el.getId(), el_compare.getId());
		assertEquals(el.getWojewodztwo().getId(), el_compare.getWojewodztwo().getId());
		assertNotEquals(el, el_compare);

	}
	
	@Test
	public void ulicaConstructorTest() {
		
		WojewodztwoEnt el_1 = new WojewodztwoEnt();
		el_1.setId(101);
		el_1.setNazwa("TestWojewodztwo");

		MiastoEnt el_2 = new MiastoEnt();
		el_2.setId(1234);
		el_2.setNazwa("TestMiasto");
		el_2.setKod_pocztowy("12-543");
		el_2.setWojewodztwo(el_1);
		
		UlicaEnt el_3 = new UlicaEnt();
		el_3.setNazwa("TestUlcia");
		el_3.setMiasto(el_2);
		
		UlicaEnt el_compare = new UlicaEnt(el_3);
		
		assertEquals(el_3.getNazwa(), el_compare.getNazwa());
		assertEquals(el_2.getNazwa(), el_compare.getMiasto().getNazwa());
		assertEquals(el_1.getNazwa(), el_compare.getMiasto().getWojewodztwo().getNazwa());
		assertNotEquals(el_3, el_compare);

	}
	
	/**
	 * Test repozytorium dla województwa
	 */
	@Test
	public void wojewodztwoAddEditDeleteTest() {
		List<?> data = session.createCriteria(WojewodztwoEnt.class)
				.add(Restrictions.eq("nazwa", "TestCase"))
				.list();
		
		int testCaseRecords = data.size();
		
		WojewodztwoEnt el;
		
		//-------------------------------
		logger.trace("Add/Insert test");
		
		el = new WojewodztwoEnt();
		el.setNazwa("TestCase");
		new LokalizacjeRep(session).setWojewodztwo(el).AddWojewodztwo();
		
		long id = el.getId();
		
		data = session.createCriteria(WojewodztwoEnt.class)
				.add(Restrictions.eq("nazwa", "TestCase"))
				.list();
		
		if(data.size() < testCaseRecords)
			fail("Nie udało się dodać rekordu");
		
		//-------------------------------
		logger.trace("Edit/Update test");
		
		el.setNazwa("TestCase2");
		
		new LokalizacjeRep(session).setWojewodztwo(el).EditWojewodztwo();
		data = session.createCriteria(WojewodztwoEnt.class)
				.add(Restrictions.eq("id", id))
				.list();

		assertEquals(1, data.size());
		assertEquals("TestCase2", ((WojewodztwoEnt)data.get(0)).getNazwa());
		
		//-------------------------------
		logger.trace("Delete/Remove test");
		
		el = new WojewodztwoEnt();
		el.setId(id);
		
		el = (WojewodztwoEnt) session.merge(el); 	// ponieważ gdzieś już w pamięci sesji jest obiekt, 
													// trzeba go połaczyć z instiejącym
		
		new LokalizacjeRep(session).setWojewodztwo(el).DeleteWojewodztwo();
		data = session.createCriteria(WojewodztwoEnt.class)
				.add(Restrictions.eq("id", id))
				.list();
		
		assertEquals(0, data.size());
		
	}
	
	/**
	 * Test dla miasta wraz z relacją województwa
	 */
	@Test
	public void miastoAddEditDeleteTest() {
		List<?> data = session.createCriteria(MiastoEnt.class)
				.add(Restrictions.eq("nazwa", "TestCase"))
				.list();
		
		int testCaseRecords = data.size();
		
		MiastoEnt el;
		WojewodztwoEnt el2;
		
		//-------------------------------
		logger.trace("Add/Insert test");
				
		el = new MiastoEnt();
		el.setNazwa("TestCase");
		el.setKod_pocztowy("41-111");
		el2 = new WojewodztwoEnt();
		el2.setNazwa("TestCaseP1");
		el.setWojewodztwo(el2);
		
		new LokalizacjeRep(session).setMiasto(el).AddMiasto();
		
		long id = el.getId();
		//long id2 = el2.getId();
		
		data = session.createCriteria(MiastoEnt.class)
				.add(Restrictions.eq("nazwa", "TestCase"))
				.list();
		
		if(data.size() < testCaseRecords)
			fail("Nie udało się dodać rekordu");
		
		//-------------------------------
		logger.trace("Edit/Update test");
		
		el.setNazwa("TestCase2");
		el2.setNazwa("TestCaseP2");
		el.setWojewodztwo(el2);
		
		new LokalizacjeRep(session).setMiasto(el).EditMiasto();
		data = session.createCriteria(MiastoEnt.class)
				.add(Restrictions.eq("id", id))
				.list();

		assertEquals(1, data.size());
		assertEquals("TestCase2", ((MiastoEnt)data.get(0)).getNazwa());
		assertEquals("TestCaseP2", ((MiastoEnt)data.get(0)).getWojewodztwo().getNazwa());
		
		//-------------------------------
		logger.trace("Delete/Remove test");
		
		el = new MiastoEnt();
		el.setId(id);
		
	
		new LokalizacjeRep(session).setMiasto(el).DeleteMiasto();
		data = session.createCriteria(MiastoEnt.class)
				.add(Restrictions.eq("id", id))
				.list();
		
		assertEquals(0, data.size());
		
	}
	
	@Test
	public void ulicaAddEditDeleteTest() {
		List<?> data = session.createCriteria(UlicaEnt.class)
				.add(Restrictions.eq("nazwa", "TestCase"))
				.list();
		
		int testCaseRecords = data.size();
		
		UlicaEnt el;
		MiastoEnt el2;
		WojewodztwoEnt el3;
		
		//-------------------------------
		logger.trace("Add/Insert test");
				
		el = new UlicaEnt();
		el.setNazwa("TestCase");
		
		el2 = new MiastoEnt();
		el2.setNazwa("TestCaseP1");
		el2.setKod_pocztowy("41-111");
		el.setMiasto(el2);
		
		el3 = new WojewodztwoEnt();
		el3.setNazwa("TestCaseQ1");
		el2.setWojewodztwo(el3);
		
		new LokalizacjeRep(session).setUlica(el).AddUlica();
		
		long id = el.getId();
		//long id2 = el2.getId();
		//long id3 = el3.getId();
		
		data = session.createCriteria(UlicaEnt.class)
				.add(Restrictions.eq("nazwa", "TestCase"))
				.list();
		
		if(data.size() < testCaseRecords)
			fail("Nie udało się dodać rekordu");
		
		//-------------------------------
		logger.trace("Edit/Update test");
		
		el.setNazwa("TestCase2");
		el2.setNazwa("TestCaseP2");
		el3.setNazwa("TestCaseQ2");
		el.setMiasto(el2);
		el2.setWojewodztwo(el3);
		
		new LokalizacjeRep(session).setUlica(el).EditUlica();
		data = session.createCriteria(UlicaEnt.class)
				.add(Restrictions.eq("id", id))
				.list();

		assertEquals(1, data.size());
		assertEquals("TestCase2", ((UlicaEnt)data.get(0)).getNazwa());
		assertEquals("TestCaseP2", ((UlicaEnt)data.get(0)).getMiasto().getNazwa());
		assertEquals("TestCaseQ2", ((UlicaEnt)data.get(0)).getMiasto().getWojewodztwo().getNazwa());
		
		//-------------------------------
		logger.trace("Delete/Remove test");
		
		el = new UlicaEnt();
		el.setId(id);
		
		new LokalizacjeRep(session).setUlica(el).DeleteUlica();
		data = session.createCriteria(UlicaEnt.class)
				.add(Restrictions.eq("id", id))
				.list();
		
		assertEquals(0, data.size());
		
	}
	
	
	/* moje stare testy - Michał
	 * @Test
	public void wojewodztwaManage() {
		logger.trace(".");
		
		List<?> data = session.createCriteria(WojewodztwoEnt.class).list();
		int numEl = data.size();
		
		WojewodztwoEnt el;
		
		// Add test
		logger.trace("Add test");
		el = new WojewodztwoEnt();
		el.setNazwa("TestCase");
		session.save(el);

		data = session.createCriteria(WojewodztwoEnt.class).list();
		
		if(data.size() != (numEl+1)) {
			fail("Nie udało się dodać wojewodztwa");
		} else if (((WojewodztwoEnt)data.get(data.size()-1)).getNazwa()
				.compareTo("TestCase") != 0) {
			fail("Wojewodztwo ma złą nazwę");
		}
		
		new LokalizacjeRep().setWojewodztwo(new WojewodztwoEnt(el)).AddWojewodztwo();
		
		data = session.createCriteria(WojewodztwoEnt.class).list();
		
		if(data.size() != (numEl+2)) {
			fail("Nie udało się dodać wojewodztwa");
		} else if (((WojewodztwoEnt)data.get(data.size()-1)).getNazwa()
				.compareTo("TestCase") != 0) {
			fail("Wojewodztwo ma złą nazwę");
		}
		
		
		
		// update test
		logger.trace("Update test");
		
		data = session.createCriteria(WojewodztwoEnt.class).list();
		el = ((WojewodztwoEnt)data.get(data.size()-1));

		el.setNazwa("TestCase2");
		session.update(el);

		long id = el.getId();
		logger.trace("id = " + Long.toString(id));
		
		data = session.createCriteria(WojewodztwoEnt.class)
				.add(Restrictions.eq("id", id))
				.list();
		
		if(data.size() == 1 
				&& ((WojewodztwoEnt)data.get(0)).getNazwa().compareTo("TestCase2") != 0) {
			fail("Aktualizacja pola niepowiodła się");
		}
		
		// delete test
		logger.trace("delete test");
		el = new WojewodztwoEnt();
		el.setId(id);
		session.flush();
		session.delete(el);
		
		
		data = session.createCriteria(WojewodztwoEnt.class).list();
		
		if(data.size() != (numEl)) {
			fail("Usunięcie niepowiodło się");
		}
	}*/
}
