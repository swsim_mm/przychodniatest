package pl.edu.swsim.zw.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.edu.swsim.zw.HibernateFactory;
import pl.edu.swsim.zw.TextTools;
import pl.edu.swsim.zw.dao.PracownicyRep;
import pl.edu.swsim.zw.entities.MiastoEnt;
import pl.edu.swsim.zw.entities.PracownikEnt;
import pl.edu.swsim.zw.entities.UlicaEnt;
import pl.edu.swsim.zw.entities.WojewodztwoEnt;

public class PracownicyTest {

	public static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	public static Session session = null;
	public static Transaction transaction = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		logger.info("Test dla klasy " 
				+ new Object() { }.getClass().getEnclosingClass().getName());
		session = HibernateFactory.getSession();
		if(session == null)
			throw new Exception("Otwarcie sesji nie udało się");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		session.close();
	}

	@Before
	public void setUp() throws Exception {
		logger.trace(TextTools.padText("setUp", "=", 45));
		transaction = session.beginTransaction();
	}

	@After
	public void tearDown() throws Exception {
		logger.trace(TextTools.padText("tearDown", "-", 45));
		transaction.rollback();
	}
	

	/**
	 * Zwracanie poprawnie listy, inaczej nie ma sensu dalej robić testów
	 */
	@Test
	public void initialTest() {
		logger.trace(".");
		
		List<PracownikEnt> PracList = new PracownicyRep().getPracownicy();
		assertNotNull(PracList);		
				
	}
	
	@Test
	public void pracownikConstructorTest() {
			
		WojewodztwoEnt el_1 = new WojewodztwoEnt();
		el_1.setId(101);
		el_1.setNazwa("TestWojewodztwo");

		MiastoEnt el_2 = new MiastoEnt();
		el_2.setId(1234);
		el_2.setNazwa("TestMiasto");
		el_2.setKod_pocztowy("12-543");
		el_2.setWojewodztwo(el_1);
		
		UlicaEnt el_3 = new UlicaEnt();
		el_3.setNazwa("TestUlcia");
		el_3.setMiasto(el_2);
		
		PracownikEnt el_r = new PracownikEnt();
		el_r.setId(456);
		el_r.setImie("Imie");
		el_r.setNazwisko("Nazwisko");
		el_r.setPesel("01234567890");
		el_r.setUlica(el_3);
				
		PracownikEnt el_compare = new PracownikEnt(el_r);
		
		assertEquals(el_r.getImie(), el_compare.getImie());
		assertEquals(el_r.getNazwisko(), el_compare.getNazwisko());
		assertEquals(el_r.getPesel(), el_compare.getPesel());
		assertEquals(el_r.getUlica().getNazwa(), el_compare.getUlica().getNazwa());
		assertNotEquals(el_r, el_compare);		
		
	}
	
	/**
	 * Test pracownika bez relacji adresu
	 */
	@Test
	public void pracownikAddEditDeleteTest() {
		List<?> data = session.createCriteria(PracownikEnt.class)
				.add(Restrictions.eq("imie", "TestCase"))
				.list();
		
		int testCaseRecords = data.size();
		
		PracownikEnt el;
		/*UlicaEnt el1;
		MiastoEnt el2;
		WojewodztwoEnt el3;*/
		
		//-------------------------------
		logger.trace("Add/Insert test");
				
		el = new PracownikEnt();
		el.setImie("TestCase");
		el.setNazwisko("TestNazwisko");
		el.setPesel("01234567890");
		el.setBudynek("123");
		el.setMieszkanie("123456");
		el.setTelefon("0500200900");
		el.setDodatkowe("Dodatki");
		el.setPwz("4564568");
		el.setUlica(null);
		
		/*
		el = new UlicaEnt();
		el.setNazwa("TestCase");
		
		el2 = new MiastoEnt();
		el2.setNazwa("TestCaseP1");
		el2.setKod_pocztowy("41-111");
		el.setMiasto(el2);
		
		el3 = new WojewodztwoEnt();
		el3.setNazwa("TestCaseQ1");
		el2.setWojewodztwo(el3);
		*/
		
		new PracownicyRep(session).setPracownik(el).AddPracownik();
		
		long id = el.getId();
		
		data = session.createCriteria(PracownikEnt.class)
				.add(Restrictions.eq("imie", "TestCase"))
				.list();
		
		if(data.size() < testCaseRecords)
			fail("Nie udało się dodać rekordu");
		
		//-------------------------------
		logger.trace("Edit/Update test");
		
		el.setImie("C2_imie");
		/*el.setNazwa("TestCase2");
		el2.setNazwa("TestCaseP2");
		el3.setNazwa("TestCaseQ2");
		el.setMiasto(el2);
		el2.setWojewodztwo(el3);*/
		
		new PracownicyRep(session).setPracownik(el).EditPracownik();
		data = session.createCriteria(PracownikEnt.class)
				.add(Restrictions.eq("id", id))
				.list();

		assertEquals(1, data.size());
		assertEquals("C2_imie", ((PracownikEnt)data.get(0)).getImie());
		//assertEquals("TestCaseP2", ((UlicaEnt)data.get(0)).getMiasto().getNazwa());
		//assertEquals("TestCaseQ2", ((UlicaEnt)data.get(0)).getMiasto().getWojewodztwo().getNazwa());
		
		//-------------------------------
		logger.trace("Delete/Remove test");
		
		el = new PracownikEnt();
		el.setId(id);
		
		new PracownicyRep(session).setPracownik(el).DeletePracownik();
		data = session.createCriteria(PracownikEnt.class)
				.add(Restrictions.eq("id", id))
				.list();
		
		assertEquals(0, data.size());
		
		
	}

}
